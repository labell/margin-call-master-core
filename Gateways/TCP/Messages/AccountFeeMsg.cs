﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class AccountFeeMsg : IJsonSerializable
    {
        private long func_call_id;
        private int user_id;
        private decimal fee_in_perc;
        private DateTime dt_made;

        internal AccountFeeMsg(long func_call_id, int user_id, decimal fee_in_perc, DateTime dt_made) //конструктор сообщения
        {
            this.func_call_id = func_call_id;
            this.user_id = user_id;
            this.fee_in_perc = fee_in_perc;
            this.dt_made = dt_made;
        }

        public string Serialize()
        {
            return JsonManager.FormTechJson((int)MessageTypes.NewAccountFee, func_call_id, user_id, fee_in_perc, dt_made);
        }
    }
}
