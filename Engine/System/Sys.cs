﻿using System;

namespace RocketCore
{
    static class Sys
    {
        //объекты системного уровня
        internal static Core core;
        internal static FixManager fixman;
        internal static FixCaster fixcast;
        internal static Processor proc;
        internal static Clockwork clock;
        internal static TcpServer tcpsrv;
                
        static void Main(string[] args)
        {
            core = new Core();  
            clock = new Clockwork();
            proc = new Processor();
            fixman = new FixManager();
            fixcast = new FixCaster();

            int[] p = { 1330, 1340, 1350 };
            if (args != null)
            {
                if (args.Length > 0 && args.Length < 4)
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        int port;
                        if (int.TryParse(args[i], out port))
                        {
                            p[i] = port;
                            Console.WriteLine("Port #" + port.ToString() + " applied");
                        }
                        else
                        {
                            Console.WriteLine("[Error] Unable to parse port number");
                        }
                    }
                }
                else Console.WriteLine("Launching TcpServer with default ports");
            }
            tcpsrv = new TcpServer(p[0], p[1], p[2]);

            //Console.WriteLine();
            //Console.WriteLine("-----------------------------------------");
            //Console.WriteLine("PERFORMING BENCHMARK...");
            //Console.WriteLine("-----------------------------------------");
            //Console.WriteLine();
            //Tester.place_orders();
            //Tester.match_orders();

            proc.Start(); //старт обработки очереди в main-thread         
        }


    }

    
}
